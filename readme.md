# SAM vLAB Devops Docker

Ist ein Repository für eine Laborübung basierend auf Virtuellen Technologien .

Als Basis dient meist eine Aktuelle Ubuntu Distribution in einer Virtuellen Umgebung bevorzugt ist Virtualbox und VMware Player sowie Workstation , sollte aber auch unter anderen Virtuellen Umgebungen wenig Probleme bereiten.

* Die Einführung und Angaben für die Übung ist unter Workshop zu finden
* Hilfe zum Setup der Übung ist unter vlab zu finden

Have Fun and learn something new.




# SAM vLab with Docker
This is a Gitlab Repository to do an easy Virtual Lab Environment with Docker ( https://www.docker.com/ ) Open Source Software.

To Follow the Instrutions of this Virtual Lab:
Open your Browser

Option 1 (Online):
  * Browse https://gitpitch.com/rstumpner/sam-vlab-devops-docker/master?grs=gitlab&t=black

Option 2 (Local):
  * git clone https://gitlab.com/rstumpner/sam-vlab-devops-docker
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

Virtual Lab Environment Setup:

Requirements:
  * 2 GB Memory (minimal)
  * 2 x CPU Cores
  * Virtualbox

vLAB Setup:
  * docker (Ubuntu 16.04 )

Automatic Setup with Vagrant (https://www.vagrantup.com/) (local):
  * Download and Install Vagrant Package for your OS
    * On Linux
      * wget https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.deb?_ga=2.257941326.439840422.1522128825-1814262215.1522128825
      * dpkg -i vagrant_2.0.3_x86_64.deb
    * On Windows
      * https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.msi?_ga=2.89037278.439840422.1522128825-1814262215.1522128825
    * on macOS
      * https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.dmg?_ga=2.257941326.439840422.1522128825-1814262215.1522128825

  * Clone this Git Repository
    * git clone https://gitlab.com/rstumpner/sam-vlab-devops-docker
  * Start the Setup the vLAB Environment with Vagrant
    ```md
       cd sam-vlab-devops-docker/vlab/vagrant/
       vagrant up
       ```
  * Check the vLAB Setup
     ```md
     vagrant status
     ```
  * Login to work with a Node
    ```md
    vagrant ssh
    ```
#### Troubleshooting
