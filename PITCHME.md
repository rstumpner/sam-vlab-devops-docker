<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->



# Docker
* Devops Docker
* Version 20180320

---
# Docker vLAB Übersicht
* Docker Einführung
* Docker First Steps
* Docker Aufgaben Level 1

---
## Docker Voraussetzungen vLAB (on-prem)
* 2 x CPU Core
* 2 GB RAM (minimal)
* 10 GB Space
* Installation of Virtualbox (https://www.virtualbox.org/)
---
## The vLAB Environment Level 1
* dockervm.local
  * (Ubuntu 16.04 / docker-ce / Port 8082 )

---
## Docker Einführung
* Ist ein Framework zum Deployment von Anwendungen spezialisiert für Webtechnologien auf Containerbasis
* Basistechnologie Container Engines ( Containerd / LXC / Jails usw.)
* Basistechnologie Copy on Write / Layerd Dateisysteme (Unionfs/Aufs/overlay2)
* Versionscontrollsystem
* Local and Remote Image Repository
---
## Docker Einführung (Architektur)

![Docker Arichiteḱtur](_images/docker-architektur.png)

<!---
* Skizze Layerd Filesystem
* Skizze Ökosystem / Repository
* Stateless
--->
---
## Setup the Virtual Lab Environment
### (Vagrant)
* git clone https://www.gitlab.com/rstumpner/sam-vlab-devops-docker
* cd sam-vlab-devops-docker/vlab/vagrant/
* vagrant up
* vagrant status
* vagrant ssh

---
## Installation of Docker
### (manual)
* Ubuntu 16.04 LTS Basis
* Install the new Docker-CE PGP Key
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
* Install the Docker-CE Repository
```
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
* Update Repositorys
```
sudo apt-get update
```
* Install Docker-CE
```
sudo apt-get install docker-ce
```
* Enable Docker at Startup
```
sudo systemctl enable docker
```
* Link:
https://docs.docker.com/install/linux/docker-ce/ubuntu/#os-requirements

---
## Manual Installation of Docker
### (Alterntive)
* sudo su
* wget -qO- https://get.docker.com/ | sh

---
# Docker First Steps
* Check Docker Version
```
docker --version (18.03.0-ce)
```
* Run your First Container (Verify Install)
```
sudo docker run hello-world
```
---
## First Real Container Work

* Starting an Interactive Docker Container
```md
sudo docker run -it ubuntu bash
```
* List Running Containers:
```md
docker ps -a
```
---
## Docker Container Playground 1
#### Is a Docker Container Persistent ???
* Interaktiven Container Starten
```md
sudo docker run -it ubuntu bash
```
* Erzeugen von Daten im Docker Container
```md
touch test
touch test123
```
* exit Container
---
## Docker Container Playground 2
* Interaktiven Container Starten
```md
sudo docker run -it ubuntu bash
```
#### Suche nach den Daten

* List Docker Containers
```md
sudo docker ps -a
```
* List Docker Container Images
```md
sudo docker images
```
---
### Commit a Stage to an Image
* Interaktiven Container Starten:
```bash
docker run -it ubuntu bash
```
* Webserver Installieren
```bash
sudo apt-get install apache2
```
* List Docker Container
```md
sudo docker ps -a
```
* Notiere die Container ID: 9a0874f113e6
* Commit Stage to Local Image Repository
```
 sudo docker commit -m "Added Apache2" -a "Roland Stumpner" 9a0874f113e6 ubuntu-with-apache2:latest
```
---
# Check the Local Repository
* List Docker Container Images
```md
sudo docker images
```
```
vagrant@vagrant-ubuntu-trusty-64:~$ sudo docker images
REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
ubuntu-lamp           latest              edf6103714ad        10 hours ago        251MB
<none>                <none>              d09db083ad85        10 hours ago        251MB
ubuntu-with-apache2   latest              0dec70d36cf6        10 hours ago        253MB
ubuntu                latest              f975c5035748        4 weeks ago         112MB
hello-world           latest              f2a91732366c        4 months ago        1.85kB
```
---
# Docker Network
##### Run a Version of your Docker Image with a Network Portmapping to the Docker Host

```
sudo docker run -p 80:80 -t -i roland/ubuntu-with-apache2 /bin/bash
```
* Check the Apache Installation with Links
```
links http://localhost
```
---
# Docker Volume
##### Mount a Directory of the Docker Host to the Container.

```
sudo docker run -d -p 80:80 -v /vlab-files/html/:/var/www/html/ -t kanboard/kanboard:latest
```

---
## Aufgaben Docker Level 1
#### Level 1

Installiere einen Web Stack mit einer Applikation

##### Beispiel:
* Linux Apache PHP (LAP)
	* Kanboard (https://kanboard.org/)

* Build the Stack with Docker a docker-build file
---
# Docker Hint for docker-build
* mkdir firstcontainer
* touch Dockerfile
* Edit Dockerfile:

```bash

 FROM ubuntu:latest
 RUN apt-get -y update && apt-get install -y apache2
 CMD apachectl -d /etc/apache2 -e info -DFOREGROUND

```

* Build your First Dockerfile
``` bash
sudo docker build -t ubuntu-lamp:latest .

```

---
# Docker Aufgaben Level 2
* More than Just a Container ( docker-compose )
* Setting the Stage ( Docker Machine)
---
# Docker Lösung
*
---
# Docker Cheat Sheet
* List Images (docker images)
* Run Latest Ubuntu Image (sudo docker run -t -i ubuntu:latest /bin/bash)
* Download a new Container ( docker pull kanoard\kanboard)
* Show Running Containers (sudo docker ps)
* Stop a Container (sudo docker stop kanoard)
* Remove Containers not running (docker rm `docker ps -aq -f status=exited`)
* Mount a Directory from Docker Host to Container (sudo docker run -d -p 80:80 -v /mnt/kanboard/data:/var/www/app/data -t kanboard/kanboard:latest)
* bash into a Container ( docker attach )
* docker exec
---
# Links:
* Docker Build File Best Practices:
https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#minimize-the-number-of-layers
* Docker on Windows (Reiner Stropek)
https://youtu.be/p9l59ZqtCJk
---
# Notizen
